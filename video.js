var videoWidth = "";
var videoHeight = "";
var canvas = document.getElementById('canvas')
var context = canvas.getContext('2d');

function onLoadFunctions() {

  initialize();

  function initialize() {
      window.addEventListener('resize', resizeCanvas, false);
      resizeCanvas();
  }

  function resizeCanvas() {
    var video = document.getElementById("video");
    videoWidth = video.offsetWidth;
    videoHeight = video.offsetHeight;
    canvas.width = videoWidth;
    canvas.height = videoHeight;
    redraw();
  }
  
  function redraw() {
    context.strokeRect(0, 0, videoWidth, videoHeight);
  }
};

window.onload = onLoadFunctions;

function screenshot() {
  var img = document.getElementById("video");
  context.drawImage(img, 0, 0, videoWidth, videoHeight);
  canvas.setAttribute('crossOrigin', '');
  img.setAttribute('crossOrigin', '');
};


function grayscale () {
  var imageData = context.getImageData(0, 0, canvas.width, canvas.height);
  let data1 = imageData.data;
  console.log(imageData);
  for (var i = 0; i < data1.length; i += 4) {
    var avg = (data1[i] + data1[i + 1] + data1[i + 2]) / 3;
    data1[i]     = avg; 
    data1[i + 1] = avg; 
    data1[i + 2] = avg; 
  }
  console.log(imageData);
  context.putImageData(imageData, 0, 0);

};

function blur () {
  console.log("hola");
};